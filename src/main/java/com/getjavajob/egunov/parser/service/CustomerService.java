package com.getjavajob.egunov.parser.service;

import com.getjavajob.egunov.parser.dao.ConcurrentCustomerParser;
import com.getjavajob.egunov.parser.dao.Parser;
import com.getjavajob.egunov.parser.entity.Customer;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class CustomerService {

    private Queue<Customer> customers;

    public CustomerService(InputStream xmlStream, InputStream contextStream) throws IOException {
        Parser customerParser = new ConcurrentCustomerParser(xmlStream, contextStream);
        customerParser.parse();
        customers = customerParser.getCustomers();

    }

    public BigDecimal getTotalOrdersSum() { //Сумма всех заказов
        BigDecimal result = new BigDecimal(0.0).setScale(2, BigDecimal.ROUND_HALF_UP);
        if (!customers.isEmpty()) {
            for (Customer customer : customers) {
                result = result.add(customer.getOrdersSum());
            }
        }
        return result;
    }

    public int getMaxCashCustomerId() { // Клиента с максимальной суммой заказов
        int result = 0;
        BigDecimal maxOrder;
        if (!customers.isEmpty()) {
            result = customers.peek().getId();
            maxOrder = customers.peek().getOrdersSum();
            for (Customer customer : customers) {
                if (customer.getOrdersSum().compareTo(maxOrder) > 0) {
                    result = customer.getId();
                    maxOrder = customer.getOrdersSum();
                }
            }
        }
        return result;
    }

    public BigDecimal getMaxOrderSum() {  // Сумму максимального заказа
        BigDecimal result = null;
        if (!customers.isEmpty()) {
            result = customers.peek().getOrdersSum();
            for (Customer customer : customers) {
                if (customer.getOrdersSum().compareTo(result) > 0) {
                    result = customer.getOrdersSum();
                }
            }
        }
        return result;
    }

    public BigDecimal getMinOrderSum() {  // Сумму минимального заказа
        BigDecimal result = null;
        if (!customers.isEmpty()) {
            result = customers.peek().getOrdersSum();
            for (Customer customer : customers) {
                if (customer.getOrdersSum().compareTo(result) < 0) {
                    result = customer.getOrdersSum();
                }
            }
        }
        return result;
    }

    public int getOrderQty() { // Количество заказов
        int result = 0;
        for (Customer customer : customers) {
            result += customer.getOrders().size();
        }
        return result;
    }

    public List<Customer> getConditionCustomers(double condition) { //Покупатели с суммой заказа > condition
        List<Customer> result = new ArrayList<>();
        for (Customer customer : customers) {
            if (customer.getOrdersSum().compareTo(new BigDecimal(condition)) >= 0) {
                result.add(customer);
            }
        }
        return result;
    }
}


