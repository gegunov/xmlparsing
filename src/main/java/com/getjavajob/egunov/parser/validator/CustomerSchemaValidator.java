package com.getjavajob.egunov.parser.validator;

import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.IOException;
import java.io.InputStream;

public class CustomerSchemaValidator {


    public static boolean validate(InputStream xmlStream, InputStream xsdStream) throws IOException {
        Source xmlSource = new StreamSource(xmlStream);
        Source xsdSource = new StreamSource(xsdStream);
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        try {
            Schema schema = schemaFactory.newSchema(xsdSource);
            Validator validator = schema.newValidator();
            validator.validate(xmlSource);
        } catch (SAXException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }
}
