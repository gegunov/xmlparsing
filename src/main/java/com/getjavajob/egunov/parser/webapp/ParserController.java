package com.getjavajob.egunov.parser.webapp;

import com.getjavajob.egunov.parser.entity.LogData;
import com.getjavajob.egunov.parser.entity.ResponseForm;
import com.getjavajob.egunov.parser.service.CustomerService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.*;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

@Controller
public class ParserController {
    public static final String ORDER_SUM = "Сумма всех заказов  ";
    public static final String CUSTOMER_WITH_MAX_ORDERS_SUM = "Id клиента с максимальной суммой заказов  ";
    public static final String MAX_ORDER_SUM = "Сумма максимального заказа  ";
    public static final String MIN_ORDER_SUM = "Сумма минимального заказа  ";
    public static final String ORDERS_QTY = "Количество заказов  ";
    public static final String AVG_ORDER_SUM = "Средняя сумма заказа  ";

    private CustomerService customerService = null;

    @RequestMapping(value = "/MainPage")
    public String loginPage() {
        return "login";
    }

    @RequestMapping("/correct")
    public String mainPage() {
        return "MainPage";
    }

    @RequestMapping("login")
    public
    @ResponseBody
    boolean auth(@ModelAttribute LogData logData) {
        Properties props = new Properties();
        String login = logData.getLogin();
        String pass = logData.getPassword();
        Connection conn = null;
        int queryRes = 0;
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            props.load(this.getClass().getResourceAsStream("/conn.properties"));
            conn = DriverManager.getConnection("jdbc:mysql://localhost/xmlparsing",
                    props.getProperty("user"), props.getProperty("pass"));
            PreparedStatement st = conn.prepareStatement("SELECT * FROM USERS WHERE name=? AND PASSWORD = ?");
            st.setString(1, login);
            st.setString(2, pass);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                queryRes++;
            }
        } catch (IOException | SQLException e) {
            e.printStackTrace();
        } finally {
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        boolean isValid = queryRes > 0;
        return isValid;
    }

    @RequestMapping(value = "/parseFile", method = RequestMethod.POST)
    public
    @ResponseBody
    ResponseForm upload(@RequestParam MultipartFile file, @RequestParam(value = "minSum", required = false) Double minSum,
                        HttpServletRequest request) {
        ServletContext context = request.getServletContext();
        ResponseForm result = new ResponseForm();

        if (!file.isEmpty()) {
            try {
                InputStream is = context.getResourceAsStream("/WEB-INF/classes/customer-config.xsd");
                customerService = new CustomerService(file.getInputStream(), is);
            } catch (IOException e) {
                e.printStackTrace();
            }
            result.setCustomerInfo(getParsingResult());
            if (minSum != null) {
                result.setConditionCustomers(customerService.getConditionCustomers(minSum));
            }
        }
        return result;
    }



    public Map<String, Number> getParsingResult() {
        Map<String, Number> result = new LinkedHashMap<>();
        BigDecimal orderSum = customerService.getTotalOrdersSum();
        int maxCashCustomerId = customerService.getMaxCashCustomerId();
        BigDecimal maxOrderSum = customerService.getMaxOrderSum();
        BigDecimal minOrderSum = customerService.getMinOrderSum();
        int orderQty = customerService.getOrderQty();
        BigDecimal orderAvgSum = orderSum.divide(new BigDecimal(orderQty), 2, BigDecimal.ROUND_HALF_UP);


        result.put(ORDER_SUM, orderSum);
        result.put(CUSTOMER_WITH_MAX_ORDERS_SUM, maxCashCustomerId);
        result.put(MAX_ORDER_SUM, maxOrderSum);
        result.put(MIN_ORDER_SUM, minOrderSum);
        result.put(ORDERS_QTY, orderQty);
        result.put(AVG_ORDER_SUM, orderAvgSum);
        return result;
    }

}
