package com.getjavajob.egunov.parser.dao;


import java.io.*;
import java.util.concurrent.ArrayBlockingQueue;

public class CustomerReaderWorker implements Runnable {
    private ArrayBlockingQueue<String> strings;
    private ByteArrayInputStream xmlStream;

    public CustomerReaderWorker(ArrayBlockingQueue<String> strings, InputStream xmlStream) {
        this.strings = strings;
        this.xmlStream = (ByteArrayInputStream) xmlStream;
        this.xmlStream.reset();
    }

    @Override
    public void run() {
        try {
            pushCustomerToQueue();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void pushCustomerToQueue() throws IOException, InterruptedException {
        BufferedReader br = new BufferedReader(new InputStreamReader(xmlStream, "UTF-8"));
        String line;
        while ((line = br.readLine()) != null) {
            if (line.trim().equals("<customer>")) {
                StringBuilder sb = new StringBuilder();
                sb.append(line.trim());
                do {
                    line = br.readLine();
                    sb.append(line.trim());
                } while (!line.trim().equals("</customer>"));
                strings.put(sb.toString());
            }
        }
    }
}
