package com.getjavajob.egunov.parser.dao;


import com.getjavajob.egunov.parser.entity.Customer;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.StringReader;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;

public class CustomerParserWorker implements Runnable {
    private BlockingQueue<String> strings;
    private CustomerXmlHandler handler;
    private Queue<Customer> customers;

    public CustomerParserWorker(ArrayBlockingQueue<String> strings, ConcurrentLinkedQueue<Customer> customers) {
        this.strings = strings;
        this.customers = customers;
    }

    @Override
    public void run() {
        try {
            parseXmlQueue();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void parseXmlQueue() throws InterruptedException {
        String str = strings.take();
        SAXParserFactory factory = SAXParserFactory.newInstance();
        handler = new CustomerXmlHandler();
        try {
            SAXParser parser = factory.newSAXParser();
            parser.parse(new InputSource(new StringReader(str)), handler);
            Queue<Customer> handlerResult = handler.getCustomers();
            if (!handlerResult.isEmpty()) {
                for (Customer customer : handlerResult) {
                    customers.add(customer);
                }
            }
        } catch (SAXException | IOException | ParserConfigurationException e) {
            e.printStackTrace();
        }

    }
}
