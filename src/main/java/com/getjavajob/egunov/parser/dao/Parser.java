package com.getjavajob.egunov.parser.dao;

import com.getjavajob.egunov.parser.entity.Customer;

import java.io.IOException;
import java.io.InputStream;
import java.util.Queue;

public abstract class Parser {
    protected CustomerXmlHandler handler;
    protected Queue<Customer> customers;
    protected InputStream inputXmlStream;
    protected InputStream inputXsdStream;

    public Parser(InputStream inputXmlStream, InputStream inputXsdStream) {
        this.inputXmlStream = inputXmlStream;
        this.inputXsdStream = inputXsdStream;
        handler = new CustomerXmlHandler();
    }

    public abstract void parse() throws IOException;

    public Queue<Customer> getCustomers() {
        return customers;
    }


}
