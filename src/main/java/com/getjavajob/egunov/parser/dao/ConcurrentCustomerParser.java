package com.getjavajob.egunov.parser.dao;

import com.getjavajob.egunov.parser.validator.CustomerSchemaValidator;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.*;


public class ConcurrentCustomerParser extends Parser {

    private static final int THREAD_QTY = 3;
    private static final int QUEUE_SIZE = 10;

    public ConcurrentCustomerParser(InputStream inputXmlStream, InputStream inputXsdStream) {
        super(inputXmlStream, inputXsdStream);
        customers = new ConcurrentLinkedQueue<>();
    }


    @Override
    public void parse() throws IOException {
        try {
            if (CustomerSchemaValidator.validate(inputXmlStream, inputXsdStream)) {
                ArrayBlockingQueue<String> strings = new ArrayBlockingQueue<>(QUEUE_SIZE);
                ExecutorService parserService = Executors.newFixedThreadPool(THREAD_QTY);
                ExecutorService readerService = Executors.newSingleThreadExecutor();
                readerService.execute(new CustomerReaderWorker(strings, inputXmlStream)); //TODO пересоздавать сервисы

                for (int i = 0; i < THREAD_QTY; i++) {
                    parserService.execute(new CustomerParserWorker(strings, (ConcurrentLinkedQueue) customers));
                }
                parserService.shutdown();
                parserService.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
            }
        } catch (InterruptedException e) {
            e.printStackTrace(); 
        }

    }

}