package com.getjavajob.egunov.parser.dao;

import com.getjavajob.egunov.parser.validator.CustomerSchemaValidator;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.io.InputStream;


public class CustomerParser extends Parser {

    public CustomerParser(InputStream inputStream, InputStream contextStream) {
        super(inputStream, contextStream);
    }

    @Override
    public void parse() {
        try {
            if (CustomerSchemaValidator.validate(inputXmlStream, inputXsdStream)) {
                SAXParserFactory factory = SAXParserFactory.newInstance();
                SAXParser parser = factory.newSAXParser();
                parser.parse(inputXmlStream, handler);
                customers = handler.getCustomers();
            }

        } catch (IOException | SAXException | ParserConfigurationException e) {
            e.printStackTrace();
        }
    }


}
