package com.getjavajob.egunov.parser.dao;

import com.getjavajob.egunov.parser.entity.Customer;
import com.getjavajob.egunov.parser.entity.Order;
import com.getjavajob.egunov.parser.entity.Position;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.Queue;


public class CustomerXmlHandler extends DefaultHandler {

    boolean isCustomer = false;
    boolean isId = false;
    boolean isName = false;
    boolean isOrder = false;
    boolean isPosition = false;
    boolean isPrice = false;
    boolean isCount = false;

    private Queue<Customer> customers = new LinkedList<>();
    private Class lastEntity;
    private Customer customer;
    private Order order;
    private Position position;

    @Override
    public void startDocument() throws SAXException {
        System.out.println("Start parsing document");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        System.out.println("Start element :" + qName);
        if (qName.equalsIgnoreCase("CUSTOMER")) {
            customer = new Customer();
            isCustomer = true;
        }
        if (qName.equalsIgnoreCase("ID")) {
            isId = true;
        }
        if (qName.equalsIgnoreCase("NAME")) {
            isName = true;
        }
        if (qName.equalsIgnoreCase("ORDER")) {
            order = new Order();
            isOrder = true;
        }
        if (qName.equalsIgnoreCase("POSITION")) {
            position = new Position();
            isPosition = true;
        }
        if (qName.equalsIgnoreCase("PRICE")) {
            isPrice = true;
        }
        if (qName.equalsIgnoreCase("COUNT")) {
            isCount = true;
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        System.out.println("End Element :" + qName);

        if (qName.equalsIgnoreCase("CUSTOMER")) {
            customers.add(customer);
        }
        if (qName.equalsIgnoreCase("ORDER")) {
            customer.addOrder(order);
        }
        if (qName.equalsIgnoreCase("POSITION")) {
            order.addPosition(position);
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        if (isCustomer) {
            lastEntity = customer.getClass();
            isCustomer = false;
        }
        if (isOrder) {
            lastEntity = Order.class;
            System.out.println(new String(ch, start, length));
            isOrder = false;
        }
        if (isPosition) {
            lastEntity = Position.class;
            System.out.println(new String(ch, start, length));
            isPosition = false;
        }
        if (isId) {
            if (lastEntity.equals(Customer.class)) {
                customer.setId(Integer.parseInt(new String(ch, start, length)));
                System.out.println("customerId" + new String(ch, start, length));
            }
            if (lastEntity.equals(Order.class)) {
                order.setId(Integer.parseInt(new String(ch, start, length)));
                System.out.println("orderId" + new String(ch, start, length));
            }
            if (lastEntity.equals(Position.class)) {
                position.setId(Integer.parseInt(new String(ch, start, length)));
                System.out.println("positionId " + new String(ch, start, length));
            }
            isId = false;
        }
        if (isName) {
            customer.setName(new String(ch, start, length));
            System.out.println(new String(ch, start, length));
            isName = false;
        }
        if (isPrice) {
            position.setPrice(new BigDecimal(Double.parseDouble(new String(ch, start, length))));
            System.out.println(new String(ch, start, length));
            isPrice = false;
        }
        if (isCount) {
            position.setCount(new BigDecimal(Integer.parseInt(new String(ch, start, length))));
            System.out.println(new String(ch, start, length));
            isCount = false;
        }
    }

    public Queue<Customer> getCustomers() {
        return customers;
    }
}
