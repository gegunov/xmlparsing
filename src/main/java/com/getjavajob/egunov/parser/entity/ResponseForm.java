package com.getjavajob.egunov.parser.entity;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;
import java.util.Map;

public class ResponseForm {
    @JsonProperty
    private Map<String, String> customerInfo;
    @JsonProperty
    private List<Customer> conditionCustomers;

    public Map getCustomerInfo() {
        return customerInfo;
    }

    public void setCustomerInfo(Map customerInfo) {
        this.customerInfo = customerInfo;
    }

    public List<Customer> getConditionCustomers() {
        return conditionCustomers;
    }

    public void setConditionCustomers(List<Customer> conditionCustomers) {
        this.conditionCustomers = conditionCustomers;
    }
}
