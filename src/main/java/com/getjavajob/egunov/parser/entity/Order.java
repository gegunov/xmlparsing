package com.getjavajob.egunov.parser.entity;

import java.util.ArrayList;
import java.util.List;

public class Order extends Element {

    private List<Position> positions = new ArrayList<>();

    public void addPosition(Position position) {
        positions.add(position);
    }

    public List<Position> getPositions() {
        return positions;
    }

}
