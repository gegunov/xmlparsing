package com.getjavajob.egunov.parser.entity;

import java.math.BigDecimal;

public class Position extends Element {
    private int id;
    private BigDecimal price;
    private BigDecimal count;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getCount() {
        return count;
    }

    public void setCount(BigDecimal count) {
        this.count = count;
    }
}
