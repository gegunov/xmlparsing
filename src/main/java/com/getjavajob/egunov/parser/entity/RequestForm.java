package com.getjavajob.egunov.parser.entity;


import java.io.File;

public class RequestForm {
    private File file;
    private int minOrderSumForDisplayCustomer;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public int getMinOrderSumForDisplayCustomer() {
        return minOrderSumForDisplayCustomer;
    }

    public void setMinOrderSumForDisplayCustomer(int minOrderSumForDisplayCustomer) {
        this.minOrderSumForDisplayCustomer = minOrderSumForDisplayCustomer;
    }
}
