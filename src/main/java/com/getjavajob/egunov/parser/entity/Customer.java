package com.getjavajob.egunov.parser.entity;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Customer extends Element {

    private String name;
    private List<Order> orders;

    public Customer() {
        orders = new ArrayList<>();
    }

    public void addOrder(Order order) {
        orders.add(order);
    }

    public int ordersQty() {
        return orders.size();
    }

    public BigDecimal getOrdersSum() {
        BigDecimal result = new BigDecimal(0.0).setScale(2, BigDecimal.ROUND_HALF_UP);
        for (Order order : orders) {
            for (Position position : order.getPositions()) {
                result = result.add(position.getCount().multiply(position.getPrice()));
            }
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Customer)) return false;

        Customer customer = (Customer) o;

        if (name != null ? !name.equals(customer.name) : customer.name != null) return false;
        if (orders != null ? !orders.equals(customer.orders) : customer.orders != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (orders != null ? orders.hashCode() : 0);
        return result;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
