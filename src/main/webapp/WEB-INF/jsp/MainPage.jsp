<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Парсинг данных о покупателях</title>


    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" type="text/css" rel="stylesheet"/>
    <link href="${pageContext.request.contextPath}/resources/css/style.css" type="text/css" rel="stylesheet"/>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/javascript/jquery-2.1.3.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/javascript/bootstrap.js"></script>

    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/javascript/xmlParsing.js"></script>

</head>
<body>

<div class="content">
    <div class="myinputstyle">
        <input type="file" class="form-control inputFile" id="file" placeholder="Выберите файл"/>
        <input type="checkbox" id="addOpts"/>

        <p>Дополнительные параметры</p>
        <button type="button" class="btn btn-primary btn-lg parseButton">Распарсить</button>
        <div class="addProps">
            <input type="checkbox" id="showCustomers"/>

            <p>Отобразить список покупателей с суммой заказа больше</p>
            <input type="number" id="minSum" class="form-control" disabled/>
        </div>
    </div>

    <div class="parsingResult">
        <table id="resultTable" cellpadding="5px" class="table-bordered">

        </table>
        <table id="conditionCustomers" class="table table-bordered">

        </table>
    </div>

</div>


</body>
</html>
