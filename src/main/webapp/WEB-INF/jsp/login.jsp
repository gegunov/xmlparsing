<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet" type="text/css">
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/javascript/jquery-2.1.3.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/javascript/login.js"></script>
</head>

<body>
<form method="POST">
    <div class="content">
        <div class="authGroup">
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1">Логин</span>
                <input type="text" class="form-control" placeholder="Username" aria-describedby="basic-addon1"
                       id="login">
            </div>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon">Пароль</span>
                <input type="text" class="form-control" placeholder="Password" aria-describedby="basic-addon"
                       id="password">
            </div>
            <button type="button" class="btn btn-primary btn-lg btn-block" id="loginForm">Войти</button>
        </div>
    </div>


</form>
</body>
</html>

