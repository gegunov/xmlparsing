$(function () {


    $('#addOpts').change(function () {
        $(this).prop('checked') ? $('.addProps').css('display', 'inline-block') : $('.addProps').css('display', 'none');
    });

    $('#showCustomers').change(function () {
        $(this).prop('checked') ? $('#minSum').prop('disabled', false) : $('#minSum').prop('disabled', true).val('');
    });

    $('.parseButton').click(function () {
        if ($('#file').val() == '') {
            alert('Выберите файл')
        } else {
            uploadFormData();
        }
    });

    function uploadFormData() {
        var formToSend = new FormData();
        formToSend.append("file", file.files[0]);
        formToSend.append("minSum", $('#minSum').val());
        ($.ajax({
            url: 'parseFile',
            data: formToSend,
            dataType: 'json',
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                $('#resultTable tr').remove();
                $('#conditionCustomers thead').remove();
                $('#conditionCustomers tbody').remove();

                for (var key in data.customerInfo) {
                    $('#resultTable').append('<tr><td>' + key + '</td><td>' + data.customerInfo[key] + '</td></tr>');
                }
                if (data.conditionCustomers != null) {
                    $('#conditionCustomers').append('<thead><tr><th>Id</th><th>Имя</th><th>Сумма заказа</th></tr></thead>');
                    $.each(data.conditionCustomers, function (i, item) {
                        $('#conditionCustomers').append('<tr><td>' + item.id + '</td><td>' + item.name + '</td><td>' + item.ordersSum + '</td></tr>');
                    })
                }
            }
        }));


    }
});
